@extends('coupon::coupons.layouts.app')
@section('content')
<script >
$(document).ready(function () {
		var data = {
                "_token": "{{ csrf_token() }}",
                //page:page
            }

		$('#fid_table').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "lengthMenu": [20, 40, 60, 80, 100],
            "pageLength": 20,
            "ajax": {
                'type': 'POST',
                "url": "{{ url('get-all-coupons') }}",
                //"dataSrc": "",
                'data': data,
                error: function (xhr, error, code)
                {
                    console.log(xhr);
                    console.log(code);
                    console.log(error);
                },
            },
            'order':[[0,'desc']],  
            "columns": [
                { data: 'sr' },
                { data: 'coupon_name' },
                { data: 'coupon_code' },
                { data: 'start_date'},
                { data: 'expiry_date'},
                { data: null, 'orderable':false },
                { data: null, 'orderable':false },
            ],
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', 'tr_'+aData['id']);
                 

                $status='In-active';
                $status_class='text-danger';
                if(aData['status']=='active'){
                	$status='Active';
                	$status_class='text-success';
                }
                $(nRow).find('td:eq(5)').html('<label class="text '+$status_class+'">'+$status+'</label');
                
                var edit_link = '';
                var edit_link_href = "{{ url('edit-coupon') }}/"+aData['id'];
                var edit_link = '<a href="'+edit_link_href+'" type="button" class="btn btn-warning btn-sm text-decoration-none" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Edit List" title="Edit"><i class="bi bi-pencil-square"></i></a>';
                $(nRow).find('td:eq(6)').html(edit_link);
            },
            
        });
	});
</script>
<div class="container-fluid mt-3">
	<div class="m-4">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/')}}" class="text-decoration-none">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Coupon List</li>
			</ol>
		</nav>
		<h4><small>Coupon List</small></h4>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card card-1 m-4">
				<form>
					<div class="row">
						<div class="col-auto offset-md-9 mb-3 mt-3">
							<div class="float-end">
								<!-- Button trigger modal -->
								<a href="{{url('add-coupon')}}" type="button" class="btn btn-info text-decoration-none" >
									<i class="bi bi-plus-lg"> </i>Add Coupon
								</a>
								<a href="{{url('apply-coupon')}}" type="button" class="btn btn-info text-decoration-none" >
									<i class="bi bi-plus-lg"> </i>Apply Coupon
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="card card-1 m-2 p-3">
				<div class="table-responsive tableFixHead">
				@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
				@endif
					<table class="table table-striped text-center" id="fid_table">
						<thead>
							<tr>
								<th scope="col">Sr.No.</th>
								<th scope="col">Coupon</th>
								<th scope="col">Coupon Code</th>
								<th scope="col">Start Date</th>
								<th scope="col">End Date</th>
								<th scope="col">Status</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection