@extends('coupon::coupons.layouts.app')
@section('content')
@php
	$SubscriptionTypeList = CouponHelpers::get_subscription_type();
	$CouponForList = CouponHelpers::get_coupon_for();
	$StatusList = CouponHelpers::status();
	$DiscountTypeList = CouponHelpers::get_discount_type();
@endphp
	<script >
		$(document).on("input", ".allow_numeric", function() {
			this.value = this.value.match(/^[0-9]*$/);
		});
		$(document).on("input", ".allow_decimal_numeric", function() {
			this.value = this.value.match(/^[0-9]+\.?[0-9]*$/);
		});
		$(document).on("input", "#discount", function() {
			this.value = this.value.match(/^[0-9]*\.?[0-9]*$/);
		});
		$(document).on("input", "#coupon_availability", function() {
			this.value = this.value.match(/^\d+$/);
		});
		function submit_form(){
			if($('.server_validation').length>0){
				$('.server_validation').remove();
			}
			var coupon_code = $('#coupon_code').val();
			var coupon_name = $('#name').val();
			var status = $('#status').val();
			var start_date = $('#StartDate').val();
			var expiry_date = $('#ExpiryDate').val();
			var min_amount = $('#min_amount').val();
			var discount_type = $('#discount_type').val();
			var discount = $('#discount').val();
			var maximum_bonus_amount = $('#maximum_bonus_amount').val();
			var coupon_availability = $('#coupon_availability').val();
			var subscription_type = $('#subscription_type').val();
			var coupon_for = $('#coupon_for').val();
			var User = $('#User').val();
			$('.all_errors').html('');
			var flag=false;
			var error_msg ='';
			if(coupon_name.length==''){
				error_msg += '<li>Coupon Name is required.</li>';
				flag = true;
			}
			if(coupon_name.length!=''){
				if(coupon_name.length<3){
					error_msg += '<li>Coupon Name should be at least 3 characters.</li>';
					flag = true;
				}
			}
			if(coupon_code.length==''){
				error_msg += '<li>Coupon Code is required.</li>';
				flag = true;
			}else{
				if(coupon_code.length<3){
					error_msg += '<li>Coupon Code should be at least 3 characters.</li>';
					flag = true;
				}
			}
			if(status==''){
				error_msg += '<li>Status is required.</li>';
				flag = true;
			}
			if(start_date.length==''){
				error_msg += '<li>Start Date is required.</li>';
				flag = true;
			}
			if(expiry_date.length==''){
				error_msg += '<li>End Date is required.</li>';
				flag = true;
			}
			/*if(min_amount.length==''){
				error_msg += '<li>Min Amount is required.</li>';
				flag = true;
			}*/
			if(discount_type==''){
				error_msg += '<li>Bonus Type is required.</li>';
				flag = true;
			}
			if(discount.length==''){
				error_msg += '<li>Bonus Amount is required.</li>';
				flag = true;
			}
			if(discount_type=='percentage'){
				if(discount>100){
					error_msg += '<li>Bonus should be less than 100, if choose Bonus type "Percentage" .</li>';
					flag = true;
				}
			}
			if(coupon_availability.length==''){
				error_msg += '<li>Usage Count is required.</li>';
				flag = true;
			}

			if(subscription_type==''){
				error_msg += '<li>User State is required.</li>';
				flag = true;
			}
			if(coupon_for==''){
				error_msg += '<li>Coupon for Users is required.</li>';
				flag = true;
			}
			if(coupon_for=='specific'){
				var User = $('#User').val();
				if(User==''){
					error_msg += '<li>User is required.</li>';
					flag = true;
				}
			}
			if(flag){
				$('.all_errors').html('<div class="alert alert-danger"><ul>'+error_msg+'</ul></div>');
				return false;
			}
		}
		function getCouponFor(value){
			$('.user_box').hide();
			$('.user_required').hide();
				if(value=='specific'){
					$('.user_box').show();
					$('.user_required').show();
				}
		}
		function getDiscountType(value){
			$('.discount_amount_text').hide();
			$('.discount_percentage_text').hide();
			if(value=='percentage'){
				$('.discount_percentage_text').show();
				$('#discount').attr('placeholder', 'Enter Bonus Percentage');
			}
			if(value=='fixed_amount'){
				$('.discount_amount_text').show();
				$('#discount').attr('placeholder', 'Enter Bonus Amount');
			}
		}
	</script>
	<div class="container-fluid mt-3">
		<div class="m-4">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{url('/')}}" class="text-decoration-none">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('coupons')}}" class="text-decoration-none">Coupon List</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
				</ol>
			</nav>
			<h2><small>{{$title}}</small></h2>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-1 m-4">
					<div class="card-header bg-white">
						<div class="text-right">
							<a href="{{url('coupons')}}" class="btn btn-primary"><i class="bi bi-arrow-return-left"></i> Back to Coupon List </a>
						</div>
					</div>
					<div class="card-body">
						<form method="post" id="coupon_form" action="{{url('coupon-save')}}" enctype="multipart/form-data">
							{!! csrf_field() !!}
							@if (isset($errors))
								@if ($errors->any())
									<div class="alert alert-danger server_validation">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							@endif
							<div class="all_errors browser_validation">
							</div>
							<div class="row">
								<div class="col-md-4 ">
									<label  class="form-label">Coupon Code <span class="text-danger coupon_code_required">*</span></label>
									<input type="text" class="form-control" maxlength="10" id="coupon_code" name="coupon_code" placeholder="Enter Coupon Code" value="{{(!empty($game_details_data))?$game_details_data->code:old('coupon_code')}}">
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">Coupon Name <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="name" name="name" placeholder="Enter Coupon Name" value="{{(!empty($game_details_data))?$game_details_data->name:old('name')}}">
								</div>
								
								<div class="col-md-4 ">
									<label  class="form-label">Status <span class="text-danger">*</span></label>
									<select class="form-select" aria-label="Default select example" id="status" name="status">
										<option value="">Select Status</option>
										@foreach($StatusList as $key => $val)
										@php
										$status_selected = '';
										if(!empty($game_details_data)){
											$status_selected = ($game_details_data->status==$key)?'selected':'';
										}else{
											$status_selected = (old('status')==strtolower($key))?'selected':'';
										}
										@endphp
										<option value="{{strtolower($key)}}" {{$status_selected}} >{{$val}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">Start Date-time <span class="text-danger">*</span></label>
									<input type="date" autocomplete="off" class="form-control" id="StartDate" name="StartDate" placeholder="Enter Start Date-time" value="{{(!empty($game_details_data))?$game_details_data->start_date:old('StartDate')}}">
								</div>
								
								<div class="col-md-4 ">
									<label  class="form-label">End Date-time <span class="text-danger">*</span></label>
									<input type="date" class="form-control" autocomplete="off" id="ExpiryDate" name="ExpiryDate" placeholder="Enter End Date-time" value="{{(!empty($game_details_data))?$game_details_data->expiry_date:old('ExpiryDate')}}">
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">Min Amount </label>
									<input type="text" class="form-control allow_decimal_numeric" id="min_amount" name="min_amount" placeholder="Enter Min Amount" value="{{(!empty($game_details_data))?$game_details_data->min_amount:old('min_amount')}}">
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">Bonus Type <span class="text-danger">*</span></label>
									<select class="form-select" id="discount_type" name="discount_type" aria-label="Default select example"  onchange="return getDiscountType(this.value);">
										<option value=''>Select Bonus Type</option>
										@foreach($DiscountTypeList as $key => $val)
										@php
										$discount_type_selected = '';
										if(!empty($game_details_data)){
											$discount_type_selected = ($game_details_data->bonus_type==$key)?'selected':'';
										}else{
											$discount_type_selected = (old('discount_type')==$key)?'selected':'';
										}
										@endphp
										<option value="{{$key}}" {{ $discount_type_selected }} >{{$val}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">Bonus @if(empty($game_details_data)) <span class="discount_amount_text" style="display:none;">Amount</span><span class="discount_percentage_text" style="display:none;">Percentage</span>
										@else
											@if($game_details_data->bonus_type=='percentage')
												<span class="discount_amount_text" style="display:none;">Amount</span><span class="discount_percentage_text" >Percentage</span>
											@else
											<span class="discount_amount_text" >Amount</span><span class="discount_percentage_text" style="display:none;">Percentage</span>
											@endif
										@endif

										<span class="text-danger">*</span></label>
									<input type="text" class="form-control allow_decimal_numeric"  id="discount" name="discount" placeholder="Enter Bonus" value="{{(!empty($game_details_data))?$game_details_data->bonus_amount:old('discount')}}">
								</div>

								<div class="col-md-4 ">
									<label  class="form-label">Maximum Bonus Amount </label>
									<input type="text" class="form-control allow_decimal_numeric" id="maximum_bonus_amount" name="maximum_bonus_amount" placeholder="Enter Maximum Bonus Amount" value="{{(!empty($game_details_data))?$game_details_data->maximum_bonus_amount:old('maximum_bonus_amount')}}">
								</div>
								
								<div class="col-md-4 ">
									<label  class="form-label">Usage Count <span class="text-danger">*</span></label>
									<input type="text" class="form-control allow_numeric" id="coupon_availability" name="coupon_availability" placeholder="Enter Usage Count" value="{{(!empty($game_details_data))?$game_details_data->usage_count:old('coupon_availability')}}">
								</div>
								<div class="col-md-4 ">
									<label  class="form-label">User State <span class="text-danger">*</span></label>
									<select class="form-select" id="subscription_type" name="subscription_type" aria-label="Default select example">
										<option value=''>Select User State</option>
										@foreach($SubscriptionTypeList as $key => $val)
										@php
										$subscription_type_selected = '';
										if(!empty($game_details_data)){
											$subscription_type_selected = ($game_details_data->user_state==$key)?'selected':'';
										}else{
											$subscription_type_selected = (old('subscription_type')==$key)?'selected':'';
										}
										@endphp
										<option value="{{$key}}" {{$subscription_type_selected}} >{{$val}}</option>
										@endforeach
									</select>
								</div>

								<div class="col-md-4 ">
									<label  class="form-label">Coupon for Users <span class="text-danger">*</span></label>
									<select class="form-select" id="coupon_for" name="coupon_for" aria-label="Default select example" onchange="return getCouponFor(this.value);">
										<option value=''>Select Coupon for Users</option>
										@foreach($CouponForList as $key => $val)
										@php
										$coupon_for_selected = '';
										if(!empty($game_details_data)){
											$coupon_for_selected = ($game_details_data->coupon_for==$key)?'selected':'';
										}else{
											$coupon_for_selected = (old('coupon_for')==$key)?'selected':'';
										}
										@endphp
										<option value="{{$key}}" {{$coupon_for_selected}} >{{$val}}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-4  user_box" style="display:none;">
									<label  class="form-label">User <span class="text-danger user_required" style="display:none">*</span></label>
									<!-- <select class="form-control" id="User" name="User" aria-label="Default select example"> -->
									<select class="form-select  " aria-label="Default select example " name="User" id="User" >
										<option value=''>Select User</option>
										@foreach($UserList as $user_key => $user_val)
										@php
										$user_selected = '';
										if(!empty($game_details_data)){
											$user_selected = ($game_details_data->user_id==$user_val->id)?'selected':'';
										}else{
											$user_selected = (old('User')==$key)?'selected':'';
										}
										@endphp
										<option value="{{$user_val->id}}" {{$user_selected}} >{{$user_val->nick_name.'('.$user_val->mobile.')'}}</option>
										@endforeach
									</select>
								</div>
								
								
								<div class="card-footer bg-white text-center">
									<input type="hidden" name="coupon_id" id="coupon_id" value="{{(!empty($game_details_data))?$game_details_data->id:old('coupon_id')}}">
									<button type="submit" class="btn btn-success save_button" onclick="return submit_form();">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection