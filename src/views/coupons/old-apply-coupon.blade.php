@extends('coupon::coupons.layouts.app')
@section('content')

<script >
	$(document).on("input", ".allow_numeric", function() {
		this.value = this.value.match(/^[0-9]*$/);
	});
	
	function submit_form(){
		
	    var coupon_code = $('#coupon_code').val();
		
		$('.all_errors').html('');
		var flag=false;
		var error_msg ='';
		if(coupon_code.length==''){
			error_msg += '<li>Coupon Code is required.</li>';
			flag = true;
		}
	
		if(flag){
			$('.all_errors').html('<div class="alert alert-danger"><ul>'+error_msg+'</ul></div>');
			return false;
		}
		
	}
</script>
<div class="container-fluid mt-3">
	<div class="m-4">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/')}}" class="text-decoration-none">Home</a></li>
				<li class="breadcrumb-item"><a href="{{url('coupons')}}" class="text-decoration-none">Coupon List</a></li>
				<li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
			</ol>
		</nav>
		<h2><small>{{$title}}</small></h2>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card card-1 m-4">
				<div class="card-header bg-white">
					<div class="text-right">
						<a href="{{url('coupons')}}" class="btn btn-primary"><i class="bi bi-arrow-return-left"></i> Back to Coupon List </a>
					</div>
				</div>
				<div class="card-body">
					<form method="post" id="coupon_form" action="{{url('check-validate-coupon')}}" enctype="multipart/form-data">
						{!! csrf_field() !!}
						@if (isset($errors))
							@if ($errors->any())
							    <div class="alert alert-danger server_validation">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
						@endif
						<div class="all_errors browser_validation">
						</div>
						<div class="row">

							<div class="col-md-4">
								<label  class="form-label">User Id<span class="text-danger user_id_required">*</span></label>
								<input type="text" class="form-control allow_numeric" maxlength="10" id="user_id"  name="user_id" placeholder="Enter User Id">
							</div>

							 <div class="col-md-4 ">
								<label  class="form-label">Amount<span class="text-danger amount_required">*</span></label>
								<input type="text" class="form-control" maxlength="10" id="amount" name="amount" placeholder="Enter Amount">
							</div>

							<div class="col-md-4">
								<label  class="form-label">Coupon Code <span class="text-danger coupon_code_required">*</span></label>
								<select class="form-control" name="coupon_code" id="coupon_code">
								<option value=''>Select Valid Coupon</option>
									@foreach ($coupons as $keyId => $coupon)
										<option value="{{$keyId}}">{{$coupon}}</option>
									@endforeach
								</select>
							</div>
							
							<div class="card-footer bg-white text-center">
								<button type="submit" class="btn btn-success save_button" onclick="return submit_form();">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection