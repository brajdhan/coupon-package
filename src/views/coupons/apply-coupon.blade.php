@extends('coupon::coupons.layouts.app')
@section('content')

<div class="container-fluid mt-3">
	<div class="m-4">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/')}}" class="text-decoration-none">Home</a></li>
				<li class="breadcrumb-item"><a href="{{url('coupons')}}" class="text-decoration-none">Coupon List</a></li>
				<li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
			</ol>
		</nav>
		<h2><small>{{$title}}</small></h2>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card card-1 m-4">
				<div class="card-header bg-white">
					<div class="text-right">
						<a href="{{url('coupons')}}" class="btn btn-primary"><i class="bi bi-arrow-return-left"></i> Back to Coupon List </a>
					</div>
				</div>
				<div class="card-body">
					<form method="post" id="coupon_form" action="{{url('apply-coupon')}}" enctype="multipart/form-data">
						{!! csrf_field() !!}
						@if (!empty($errors))
							<div class="alert alert-danger server_validation">
							    <ul>
							        @foreach ($errors->all() as $error)
							            <li>{{ $error }}</li>
							        @endforeach
							    </ul>
							    </div>
							@endif
						   @if (!empty($errorData))
							<div class="alert alert-danger server_validation">
							    <ul>
							        <li>{{ $errorData['message'] }}</li>
							    </ul>
							    </div>
							@endif
						
							@if(isset($success) && !empty($success))
								<span id="redeem_user_id" style="display: none">{{$success['user_id']}}</span>
								<div class="alert alert-success server_validation">
									<ul>
										<li>Coupon Code :<span id="coupon_code"> {{ $success['coupon_code']}}</span></li>
										<li>Bonus : <span id="bonus">{{ ucwords($success['bonus'])}}</span></li>
										<li>Bonus Type : <span id="bonus_type">{{ ucwords($success['bonus_type'])}}</span></li>
										<li>Coupon Amount : <span id="coupon_amount">{{ $success['coupon_amount']}}</span></li>
										<li>Recharge Amount : <span id="recharge_amount">{{ $success['recharge_amount']}}</span></li>
										<li>Message : {{ $success['message']}}</li>
									</ul>
									<span class="btn btn-primary btn-sm" onclick="return redeemCoupon();">Redeem Coupon</span>
									<span id="redeem_message"></span>
								</div>
							@endif

						<div class="all_errors browser_validation">
						</div>
						<div class="row">

							<div class="col-md-4">
								<label  class="form-label">User Id<span class="text-danger user_id_required">*</span></label>
								<input type="text" class="form-control allow_numeric" maxlength="10" id="user_id"  name="user_id" value="{{old('user_id')}}" placeholder="Enter User Id">
							</div>

							 <div class="col-md-4 ">
								<label  class="form-label">Amount<span class="text-danger amount_required">*</span></label>
								<input type="text" class="form-control" maxlength="10" id="amount" name="amount" value="{{old('amount')}}" placeholder="Enter Amount" >
							</div>

							<div class="col-md-4">
								<label  class="form-label">Coupon Code <span class="text-danger coupon_code_required">*</span></label>
								<select class="form-control" name="coupon_code" id="coupon_code">
								<option value=''>Select Valid Coupon</option>
									@foreach ($coupons as $keyId => $coupon)
										<option value="{{$keyId}}" @if(old('coupon_code') == $keyId) selected @endif>{{$coupon}}</option>
									@endforeach
								</select>
							</div>
							
							<div class="card-footer bg-white text-center">
								<button type="submit" class="btn btn-success save_button" onclick="return submit_form();">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script >
	$(document).on("input", ".allow_numeric", function() {
		this.value = this.value.match(/^[0-9]*$/);
	});
	function submit_form(){
		if($('.server_validation').length>0){
			$('.server_validation').remove();
		}

	    var user_id = $('#user_id').val();
	    var amount = $('#amount').val();
	    var coupon_code = $('#coupon_code').val();
		
		$('.all_errors').html('');
		var flag=false;
		var error_msg ='';
		if(coupon_code.length==''){
			error_msg += '<li>Coupon Code is required.</li>';
			flag = true;
		}

		if(amount.length==''){
			error_msg += '<li>Amount Is Required.</li>';
			flag = true;
			console.log("amount"+amount);
		}
	
		if(flag){
			$('.all_errors').html('<div class="alert alert-danger"><ul>'+error_msg+'</ul></div>');
			return false;
		}
	}

	function redeemCoupon()
	{
		//alert('redeemCoupon');
		$('#redeem_message').hide();

		const user_id = $('#redeem_user_id').text();
		const coupon_code = $('#coupon_code').text();
		const recharge_amount = $('#recharge_amount').text();
		const bonus_amount = $('#coupon_amount').text();
	    const formData = {'user_id':user_id,'coupon_code':coupon_code, "recharge_amount":recharge_amount, "bonus_amount":bonus_amount}
    
		$.ajax({
			url: "{{ route('coupon.redeem-coupon') }}",
			type: "GET",
            data: formData,
            dataType: "json",
			success: function (response) {
			// Display success message on successful API response
				console.log(response);
				if(response.error === false){
					$('#redeem_message').text(response.message).addClass('text-success').removeClass('error');
				}else{
					$('#redeem_message').text(response.message).addClass('text-danger').removeClass('error');
				}
				$('#redeem_message').show();
			},
			error: function (xhr, status, error) {
			// Display error message on API request failure
				var response = JSON.parse(xhr.responseText);
				var errorMessage = response.message || 'Something went wrong. Please try again later.';
				$('#response-message').text(errorMessage).addClass('error').removeClass('success');
			}
        });
	}
</script>

@endsection