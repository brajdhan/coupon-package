<?php

namespace Selftech\Coupon;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;

class CouponServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        // $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'coupon');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        
        $this->app->make('Selftech\Coupon\Http\Controllers\CouponController');
        $this->loadViewsFrom(__DIR__.'/views', 'coupon');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views'),
            __DIR__.'/database/migrations' => base_path('database/migrations'),
            //__DIR__.'/Http/Controllers' => base_path('app/Http/Controllers'),
            //__DIR__.'/Helper' => base_path('app/Helper'),
        ]);


        $this->publishes([
        ]);
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        include __DIR__.'/routes/routes.php';
        include __DIR__.'/routes/api.php';
        
         // Apply CSRF protection to the package's routes
         $this->app->make('router')->middleware('web');
    }
}
