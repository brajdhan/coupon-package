<?php

namespace Selftech\Coupon\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Selftech\Coupon\Models\Coupon;
use Selftech\Coupon\Models\RedeemCoupon;

class CouponController extends Controller
{

    public function index()
    {
        $data['title'] = 'Coupons';
        return view('coupon::coupons.coupon-list', $data);
    }
    /* This api used to get coupon list */
    public function getAllCoupons(Request $request)
    {
        $errors = array();
        $success_msg = "";
        $CouponList = Coupon::select('*');
        if ($request->search['value'] != '') {
            $Search = $request->search['value'];
            $CouponList->where(function ($CouponList) use ($Search) {
                $CouponList->where('name', 'LIKE', "%{$Search}%");
                $CouponList->orWhere('code', 'LIKE', "%{$Search}%");
            });
        }
        $ResultCount = $CouponList->get();
        $coupon_count = $ResultCount->count();
        $CouponResult =  $CouponList;
        if ($request->length)
            $CouponResult = $CouponResult->offset($request->start)->limit($request->length);
        $desc = '';
        if ($request->order) {
            //print_r($request->order);
            foreach ($request->order as $order) {
                $Data['column'] = $order['column'];
                $Data['order_by'] = $order['dir'];
            }
            switch ($Data['column']) {
                case 0:
                    $CouponResult = $CouponResult->orderBy('id', $Data['order_by']);
                    $desc = $Data['order_by'];
                    break;
                case 1:
                    $CouponResult = $CouponResult->orderBy('name', $Data['order_by']);
                    break;
                case 2:
                    $CouponResult = $CouponResult->orderBy('code', $Data['order_by']);
                    break;
                case 3:
                    $CouponResult = $CouponResult->orderBy('start_date', $Data['order_by']);
                    break;
                case 4:
                    $CouponResult = $CouponResult->orderBy('expiry_date', $Data['order_by']);
                    break;
            }
        }
        $CouponResult = $CouponResult->get();
        $arr = [];
        $StartLength = 0;
        if ($desc != '') {
            if ($desc == 'asc' || $desc == 'ASC') {
                $StartLength = $coupon_count;
                if ($request->start != '')
                    $StartLength = $StartLength - $request->start;
            } else {
                if ($request->start != '')
                    $StartLength = $request->start + 1;
            }
        } else {
            if ($request->start != '')
                $StartLength = $request->start + 1;
        }

        $sr = $StartLength;
        foreach ($CouponResult as $ut) {
            $arr[] = array(
                'sr' => $sr,
                'id' => $ut->id,
                'coupon_name' => $ut->name,
                'coupon_code' => $ut->code,
                'start_date' => $ut->start_date,
                'expiry_date' => $ut->expiry_date,
                'status' => $ut->status,
            );
            if ($desc != '') {
                if ($desc == 'asc' || $desc == 'ASC') {
                    $sr--;
                } else {
                    $sr++;
                }
            } else {
                $sr++;
            }
        }
        $response = array('draw' => $_REQUEST['draw'], 'recordsTotal' => $coupon_count, 'recordsFiltered' => $coupon_count, 'data' => $arr);
        echo json_encode($response);
    }
    /* This api used to delete game by id */
    public function deleteCoupon($coupon_id = null)
    {
        $errors = array();
        $success_msg = '';
        try {
            $coupon = Coupon::where('id', $coupon_id)->first();
            if ($coupon) {
                $coupon->is_delete = 'yes';
                $coupon->update();
                $success_msg = array('msg' => 'Coupon deleted successfully.');
            } else {
                $errors = array('msg' => 'Data not found');
            }
        } catch (\Exception $e) {
            $errors = array('msg' => 'Something went wrong, Please try again!');
        }

        if (count($errors) > 0) {
            return $response = json_encode(['status' => false, 'message' => $errors['msg']]);
        } else {
            return $response = json_encode(['status' => true, 'message' => $success_msg['msg']]);
        }
    }

    public function add_coupon()
    {
        $data['UserList'] = User::get();
        $data['title'] = 'Add Coupon';
        return view('coupon::coupons.add-coupons', $data);
    }
    public function edit($CouponId)
    {
        $coupon_details = $this->getCouponById($CouponId);
        $decode_coupon_details = json_decode($coupon_details);
        $CouponDetailsArr = array();
        if ($decode_coupon_details->status == 'active') {
            $CouponDetailsArr = $decode_coupon_details->data;
        }
        $data['game_details_data'] = $CouponDetailsArr;
        $data['UserList'] = User::get();
        $data['title'] = 'Edit Coupon';
        $data['CouponId'] = $CouponId;
        return view('coupon::coupons.add-coupons', $data);
    }
    /* This api used to get game by id */
    public function getCouponById($coupon_id = null)
    {
        $errors = array();
        $success_msg = "";
        try {
            $coupon = Coupon::select('*')
                ->where('id', $coupon_id)
                ->first();

            if ($coupon) {
                $success_msg = array('msg' => 'Data found');
            } else {
                $errors = array('msg' => 'Data not found');
            }
        } catch (\Exception $e) {
            $errors = array('msg' => 'Something went wrong, Please try again!');
        }

        if (count($errors) > 0) {
            return $response = json_encode(['status' => false, 'message' => $errors['msg']]);
        } else {
            return $response = json_encode(['status' => true, 'message' => $success_msg['msg'], 'data' => $coupon]);
        }
    }

    /* This api used to save coupon */
    public function save_coupon(Request $request)
    {
        $errors = array();
        $success_msg = array();
        if ($request->input('coupon_id')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:255|unique:coupons,name,' . $request->input('coupon_id'),
                'status' => 'required|in:active,inactive',
                'coupon_code' => 'required|min:3|max:10',
                'StartDate' => 'required',
                'ExpiryDate' => 'required',
                'discount_type' => 'required|in:percentage,fixed_amount',
                'discount' => 'required|numeric',
                'coupon_availability' => 'required|integer',
                'subscription_type' => 'required',
                'coupon_for' => 'required',
            ], [
                'coupon_availability.required'    => 'The Usage Count is required.',
                'subscription_type.required'    => 'The User State is required.',
                'discount_type.required'    => 'The Bonus Type is required.',
                'discount.required'    => 'The Bonus Amount is required.',
                'coupon_for.required'    => 'The Coupon for Users is required.',
            ]);
            if ($request->input('coupon_type') == 'coupon')
                $checkeName = Coupon::where('code', $request->input('coupon_code'))
                    ->where('coupon_type', 'coupon')
                    ->where('id', '!=', $request->input('coupon_id'))
                    ->count();
            else
                $checkeName = 0;
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:coupons|min:3|max:255',
                'status' => 'required',
                //'coupon_code' => 'min:3|max:10',
                'StartDate' => 'required',
                'ExpiryDate' => 'required',
                'discount' => 'required',
                'coupon_availability' => 'required',
                'subscription_type' => 'required',
                'coupon_for' => 'required',
                'subscription_type' => 'required',
            ], [
                'coupon_availability.required'    => 'The Usage Count is required.',
                'subscription_type.required'    => 'The User State is required.',
                'discount_type.required'    => 'The Bonus Type is required.',
                'discount.required'    => 'The Bonus Amount is required.',
                'coupon_for.required'    => 'The Coupon for Users is required.',
            ]);
            if ($request->input('coupon_type') == 'coupon')
                $checkeName = Coupon::where('code', $request->input('coupon_code'))
                    ->where('coupon_type', 'coupon')
                    ->count();
            else
                $checkeName = 0;
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->getMessageBag())->withInput();
        } else {
            try {
                if ($checkeName == 0) {
                    if ($request->input('coupon_id')) {
                        $coupon = Coupon::find($request->input('coupon_id'));
                    } else {
                        $coupon = new Coupon;
                    }
                    $coupon->name = $request->name;
                    $coupon->code = strtoupper($request->coupon_code);
                    $coupon->status = $request->status;
                    $coupon->start_date = $request->StartDate;
                    $coupon->expiry_date = $request->ExpiryDate;
                    $coupon->min_amount = $request->min_amount;
                    $coupon->bonus_amount = $request->discount;
                    $coupon->bonus_type = $request->discount_type;
                    $coupon->maximum_bonus_amount = $request->maximum_bonus_amount;
                    $coupon->usage_count = $request->coupon_availability;
                    $coupon->user_state = $request->subscription_type;
                    $coupon->coupon_for = $request->coupon_for;
                    $coupon->user_id = $request->User;
                    if ($coupon->save()) {
                        if ($request->input('coupon_id')) {
                            $success_msg = array('msg' => 'Coupon updated successfully.');
                        } else {
                            $success_msg = array('msg' => 'Coupon added successfully.');
                        }
                    } else {
                        $errors = array('msg' => 'failed');
                    }
                } else {
                    $errors = array('msg' => 'Coupon already exist');
                }
            } catch (\Exception $e) {
                $errors = array('msg' => 'Something went wrong, Please try again!');
            }
        }
        if (count($errors) > 0) {
            //return $response = json_encode(['status' => false, 'message' => $errors['msg']]);
            Session::flash('message', $errors['msg']);
            Session::flash('alert-class', 'alert-danger');
            if ($request->input('coupon_id')) {
                return redirect('edit-coupon/' . $request->input('coupon_id'));
            } else {
                return redirect('add-coupon');
            }
        } else {
            //return $response = json_encode(['status' => true, 'message' => $success_msg['msg']]);
            Session::flash('message', $success_msg['msg']);
            Session::flash('alert-class', 'alert-success');
            return redirect('coupons');
        }
    }

    public function applyCouponOld(Request $request)
    {
        $data['title'] = 'Apply Coupon';
        return view('coupon::coupons.apply-coupon', $data);
    }

    /* Start Work  *Raj* */
    // apply Coupon And Validate Coupon
    public function applyCoupon(Request $request)
    {
        $errors = array();
        $errorData = array();
        $success = array();
        $title = 'Apply Coupon';
        $couponAmount = 0;
        $coupons = Coupon::where('status', 'active')->where('expiry_date', '>=', now()->toDateString())->pluck('code', 'id')->toArray();
        $validateCoupon = [];

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'amount' => 'required|numeric',
                'user_id' => 'nullable|numeric',
                'coupon_code' => 'required',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
            } else {

                if (auth()->id()) {
                    $userId = auth()->id();
                } else {
                    $userId = $request->user_id;
                }
                if ($request->user_id == null) {
                    $userId = 1;
                }

                $validateCoupon = Coupon::where(fn ($query) => $query->whereNull('user_id')->orWhere('user_id', $userId))->where('expiry_date', '>=', now()->toDateString())->where('status', 'active')->find($request->coupon_code);

                if ($validateCoupon) {

                    $couponCount = RedeemCoupon::where('coupon_id', $request->coupon_id)->where('status', 'success')->count();
                    if ($validateCoupon->usage_count >= $couponCount) {
                        if ($request->has('amount')) {
                            if ($request->amount >= $validateCoupon->min_amount) {

                                if ($validateCoupon->bonus_type === 'percentage') {
                                    $couponAmount =  $request->amount * ($validateCoupon->bonus_amount / 100);
                                } elseif ($validateCoupon->bonus_type === 'fixed_amount') {
                                    $couponAmount = $validateCoupon->bonus_amount;
                                }
                                if ($couponAmount > $validateCoupon->maximum_bonus_amount) {
                                    $couponAmount = $validateCoupon->maximum_bonus_amount;
                                }
                            }
                            $success = array(
                                'user_id' =>  $userId,
                                'coupon_code' => $validateCoupon->code,
                                'bonus' => $validateCoupon->bonus_amount,
                                'bonus_type' => $validateCoupon->bonus_type,
                                'coupon_amount' => $couponAmount,
                                'recharge_amount' => $request->amount,
                                'message' => 'Coupon Validate Successfully.'
                            );
                        } else {
                            $errorData = array('message' => 'Coupon Is Expire.');
                        }
                    } else {
                        $errorData = array('message' => 'Coupon Is Expire.');
                    }
                } else {
                    $errorData = array('message' => 'Sorry This coupon Not for You.');
                }
            }
            return view('coupon::coupons.apply-coupon', compact('title', 'coupons', 'errors', 'errorData', 'success'));
        }
        return view('coupon::coupons.apply-coupon', compact('title', 'coupons', 'errors', 'errorData', 'success'));
    }

    //API Work
    /* This Api used for display user coupon list */
    public function allCouponsList()
    {
        $error = array();
        $success = array();
        try {
            $coupons = Coupon::where(function ($query) {
                $query->whereNull('user_id')->orWhere('user_id', Auth::id());
            })->where('expiry_date', '>=', now()->toDateString())->where('status', 'active')->paginate();

            if ($coupons->total() > 0) {
                foreach ($coupons as $coupon) {
                    $data[] = $coupon;
                }
                if (count($data) > 0) {

                    $response = [
                        'current_page' => $coupons->currentPage(),
                        'hasMorePages' => $coupons->hasMorePages(),
                        'last_page' => $coupons->lastPage(),
                        'per_page' => $coupons->perPage(),
                        'total' => $coupons->total(),
                        'coupons' => $data
                    ];
                    $success = array('message' => config('message.data-found'));
                } else {
                    $error = array('message' => config('message.data-not-found'));
                }
            } else {
                $error = array('message' => config('message.data-not-found'));
            }
        } catch (\Exception $e) {
            $error = array('message' => config('message.catch-error'));
        }

        if (count($error) > 0) {
            return response()->json(['is_auth' => true, 'status' => false, 'message' => $error['message']]);
        } else {
            return response()->json(['is_auth' => true, 'status' => true, 'message' => $success['message'], 'data' => $response]);
        }
    }

    /* apply Coupons (validate-coupon) */
    public function validateCoupon(Request $request)
    {
        $error = array();
        $success = array();
        $couponAmount = 0;
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'user_id' => 'required',
            'coupon_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $error = array('message' => $validator->errors()->first());
        } else {
            try {
                $userId = $request->user_id;
                $validateCoupon = Coupon::where(fn ($query) => $query->whereNull('user_id')->orWhere('user_id', $userId))->where('expiry_date', '>=', now()->toDateString())->where('status', 'active')->find($request->coupon_id);

                if ($validateCoupon) {
                    $couponCount = RedeemCoupon::where('coupon_id', $request->coupon_id)->where('status', 'success')->count();
                    if ($validateCoupon->usage_count >= $couponCount) {
                        if ($request->has('amount')) {
                            if ($request->amount >= $validateCoupon->min_amount) {

                                if ($validateCoupon->bonus_type === 'percentage') {
                                    $couponAmount =  $request->amount * ($validateCoupon->bonus_amount / 100);
                                } elseif ($validateCoupon->bonus_type === 'fixed_amount') {
                                    $couponAmount = $validateCoupon->bonus_amount;
                                }
                                if ($couponAmount > $validateCoupon->maximum_bonus_amount) {
                                    $couponAmount = $validateCoupon->maximum_bonus_amount;
                                }
                            }
                            $data = ['coupon_id' => $validateCoupon->id, 'coupon_amount' => $couponAmount];
                            $success = array('message' => 'Data Found');
                        } else {
                            $error = array('message' => 'This Coupon Is Expire.');
                        }
                    } else {
                        $error = array('message' => 'This Coupon Is Expire.');
                    }
                } else {
                    $error = array('message' => 'Data Not Found');
                }
            } catch (\Exception $e) {
                $error = array('message' => 'Something Went Wrong, Please Try Again!');
            }
        }
        if (count($error) > 0) {
            return response()->json(['is_auth' => true, 'status' => false, 'message' => $error['message']]);
        } else {
            return response()->json(['is_auth' => true, 'status' => true, 'message' => $success['message'], 'data' => $data]);
        }
    }

    /* Redeem Coupon */
    public function redeemCoupon(Request $request)
    {
        if (auth()->id()) {
            $userId = auth()->id();
        } else {
            $userId = $request->user_id;
        }

        try {
            /* required user id coupon id amount,recharge_amount, bonus_amount first validate then redeem coupon*/
            $validateCoupon = Coupon::where(fn ($query) => $query->whereNull('user_id')->orWhere('user_id', $userId))
                ->where('expiry_date', '>=', now()->toDateString())
                ->where('status', 'active')
                ->where('code', $request->coupon_code)->first();

            if ($validateCoupon) {
                $redeemCouponCount = RedeemCoupon::where('coupon_id', $validateCoupon->id)->where('status', 'success')->count();
                if($redeemCouponCount <= $validateCoupon->usage_count)
                {
                    $couponData = RedeemCoupon::where('coupon_id', $validateCoupon->id)->where('user_id', $userId)->where('status', 'success')->first();
                    if ($couponData == null) {
                        $redeemCoupon = new RedeemCoupon();
                        $redeemCoupon->coupon_id = $validateCoupon->id;
                        $redeemCoupon->user_id = $userId;
                        $redeemCoupon->user_transaction_id = 1;
                        $redeemCoupon->redeem_date = Carbon::now();
                        $redeemCoupon->recharge_amount = $request->recharge_amount;
                        $redeemCoupon->bonus_amount = $request->bonus_amount;
                        $redeemCoupon->status = 'success';
                        $redeemCoupon->save();
                        return response()->json(['error' => false, 'message' => 'Coupon Redeemed Successfully.']);
                    } else {
                        return response()->json(['error' => true, 'message' => 'Sorry, Coupon Already You Used.']);
                    }
                }else{
                    return response()->json(['error' => true, 'message' => 'Sorry, Coupon Limit Expire.']);
                }
            }
            return response()->json(['error' => true, 'message' => 'Sorry, Coupon is Expire.']);
        } catch (\Throwable $th) {
            return response()->json(['error' => true, 'message' => $th->getMessage()]);
        }
    }
    /* End Work  *Raj* */
}