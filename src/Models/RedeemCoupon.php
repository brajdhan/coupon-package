<?php

namespace Selftech\Coupon\Models;

use Illuminate\Database\Eloquent\Model;

class RedeemCoupon extends Model
{
    protected $table = 'redeem_coupons';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'coupon_id',
        'user_id',
        'user_transaction_id',
        'redeem_date',
        'recharge_amount',
        'bonus_amount',
        'status'
    ];

    protected $hidden = ['created_at', 'updated_at'];

}
