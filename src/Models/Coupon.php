<?php

namespace Selftech\Coupon\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 
        'user_id', 
        'name', 
        'code', 
        'start_date', 
        'expiry_date',
        'min_amount',
        'bonus_amount',
        'bonus_type',
        'maximum_bonus_amount',
        'usage_count',
        'user_state',
        'coupon_for', 
        'status', 
    ];
    protected $hidden = ['created_by', 'updated_by','created_at', 'updated_at'];
    
    protected function serializeDate($date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
