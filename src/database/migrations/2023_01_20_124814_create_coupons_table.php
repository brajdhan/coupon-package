<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable();
            $table->string('name');
            $table->string('code');
            $table->date('start_date');
            $table->date('expiry_date');
            $table->integer('usage_count')->nullable();
            $table->double('min_amount',10,2)->nullable();
            $table->double('maximum_bonus_amount',10,2)->nullable();
            $table->enum('bonus_type',['percentage','fixed_amount']);
            $table->double('bonus_amount',10,2);
            $table->string('user_state');
            $table->enum('coupon_for',['all','individual'])->default('all');
            $table->enum('status',['active', 'inactive'])->default('inactive');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
};
