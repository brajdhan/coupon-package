<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem_coupons', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coupon_id');
            $table->bigInteger('user_id');
            $table->bigInteger('user_transaction_id')->nullable();
            $table->timestamp('redeem_date');
            $table->double('recharge_amount',10,2);
            $table->double('bonus_amount',10,2);
            $table->enum('status', ['success', 'failed'])->default('failed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem_coupons');
    }
};
