<?php
namespace Selftech\Coupon\Helper;

class CouponHelpers {
    
    public static function status(){
        return array('active'=>'Active', 'inactive'=>'In-Active');
    }
    
    public static function get_subscription_type(){
        return array('all users'=>'All Users', 'new users'=>'New Users', 'wallet empty'=>'Wallet Empty');
    }
    public static function get_coupon_for(){
        return array('all'=>'All', 'specific'=>'Specific');
    }
    public static function get_discount_type(){
        return array('percentage'=>'Percentage', 'fixed_amount'=>'Fixed Amount');
    }
    public static function get_coupon_type(){
        return array('coupon'=>'Coupon', 'offer'=>'Offer');
    }
}
?>