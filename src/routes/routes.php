<?php

use Illuminate\Support\Facades\Route;
use Selftech\Coupon\Http\Controllers\CouponController;

//! Web
Route::get('coupons', [CouponController::class, 'index']);
Route::get('add-coupon', [CouponController::class, 'add_coupon']);
Route::get('change-coupon-status/{id}', [CouponController::class, 'changeCouponStatus']);
Route::get('coupon-view/{id}', [CouponController::class, 'view']);
Route::get('edit-coupon/{id}', [CouponController::class, 'edit']);
Route::get('delete-coupon/{id}', [CouponController::class, 'delete_coupon']);
Route::post('coupon-save', [CouponController::class, 'save_coupon']);
Route::post('get-all-coupons', [CouponController::class, 'getAllCoupons']);
Route::post('coupon_applied', [CouponController::class, 'AppliedCoupon']);

//! Web
//* Validate Coupon
Route::match(['GET', 'POST'],'apply-coupon', [CouponController::class, 'applyCoupon']);
Route::get('redeem-coupon', [CouponController::class, 'redeemCoupon'])->name('coupon.redeem-coupon');

?>