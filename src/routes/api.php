<?php

use Illuminate\Support\Facades\Route;
use Selftech\Coupon\Http\Controllers\CouponController;

//! Coupon API
Route::group(['prefix' => 'api/v1'], function () {
    Route::controller(CouponController::class)->group(function () {
        Route::get('all-coupons-list', 'allCouponsList');
        Route::post('validate-coupon', 'validateCoupon');
    });
});