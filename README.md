# coupon-package

## Getting started

cd existing_repo
git remote add origin ['https://gitlab.com/brajdhan/coupon-package.git']
git branch -M main
git push -uf origin main

## Integrate with your Project

    Step 1 In Laravel Project directory install package using this command ['composer require selftech/coupon']

    Step 2 After Install in Config/app.php add service provider and CouponHelpers class.
        providers =  ['\Selftech\Coupon\CouponServiceProvider::class']
        aliases   =  [''CouponHelpers'=>\Selftech\Coupon\Helper\CouponHelpers::class']
    
    Step 3 publish the vendor using this command  ['php artisan vendor:publish']

    Step 4 After publish create database and run ['php artisan migrate']

    Step 5 Run the project ['php artisan serve]

    Step 6 Hit the http://127.0.0.1:8000/coupons